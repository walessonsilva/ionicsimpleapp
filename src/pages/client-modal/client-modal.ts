import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

// Services
import { ClientServiceProvider } from '../../providers/client-service/client-service';
import { NotifyProvider } from '../../providers/notify/notify';

// Models
import { Client } from '../../models/clients/client';

@IonicPage()
@Component({
  selector: 'page-client-modal',
  templateUrl: 'client-modal.html',
})
export class ClientModalPage {

    titleModal:string   = 'Adicionar Novo Cliente';
    client:Client       = new Client();

    constructor(
        public navCtrl:         NavController, 
        public navParams:       NavParams,
        private clientService:  ClientServiceProvider,
        private notify:         NotifyProvider,
        private view:           ViewController
    ) {
        // empty
    }

    ionViewDidLoad() {
        this.client = this.navParams.get('client') || new Client();
    }

    saveClient(client:Client) {
        
        let response = this.validateManualy(client);
        
        if( !(response === true) ) {
            const message = {
                title: 'Dados Inválidos',
                subTitle: response
            };

            this.notify.showMessage(message);
            return false;
        }

        if(this.client.hasOwnProperty('id') && this.client.id > 0) {
            this.update(this.client);
            return true;
        }

        this.add(this.client);
    }

    /**
     * Poderia ser utilizado o FormBuilder com o método group e add os validators
     * Porém, acredito que criar uma validação bem básica mostrar uma alternativa
     * caso não houvesse os Helpers.
     */
    private validateManualy(client:Client) {
        if(!client.name) {
            return 'O nome é obrigatório!';
        }

        if(!client.cpf) {
            return 'O CPF é obrigatório!';
        }

        if(!client.birthdate) {
            return 'A data de nascimento é obrigatória!';
        }

        return true;
    }

    private add(client:Client) {
        this.clientService.store(client).subscribe((response:any) => {
            if(!response) {
                const message:any = {
                    title: 'Erro ao atualizar Cliente!',
                    subTitle: ''
                };
                this.notify.showMessage(message);
                return false;
            }
            this.view.dismiss(response);
        }, (response) => {
            const msgError:string = this.clientService.anyError(response);
            const message:any = {
                title: 'Erro ao Adicionar Cliente!',
                subTitle: msgError
            };
            this.notify.showMessage(message);
        });
    }

    private update(client:Client) {
        
        const id:number = client.id;
        delete client.id;
        delete client.cpf;

        this.clientService.update(client, id).subscribe((response:any) => {
            if(!response) {
                const message:any = {
                    title: 'Erro ao atualizar Cliente!',
                    subTitle: ''
                };
                this.notify.showMessage(message);
                return false;
            }
            this.view.dismiss(response);
        }, (response) => {
            const msgError:string = this.clientService.anyError(response);
            const message:any = {
                title: 'Erro ao Atualizar Cliente!',
                subTitle: msgError
            };
            this.notify.showMessage(message);
        });
    }

    closeModal() {
        this.view.dismiss(false);
    }

}
