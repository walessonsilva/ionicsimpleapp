import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Services
import { ApiServiceProvider } from '../api-service/api-service';

// Models
import { Client } from '../../models/clients/client';

@Injectable()
export class ClientServiceProvider {

    private baseUrl:string = ApiServiceProvider.baseUrl;
    private headers: HttpHeaders;

    constructor(
        private http: HttpClient,
        private apiService: ApiServiceProvider
    ) {
        this.headers = this.apiService.header;
    }

    list() {
        return this.http.get(this.baseUrl+'customers', { headers: this.headers});
    }

    store(client:Client) {
        return this.http.post(this.baseUrl+'customers', client, { headers: this.headers});
    }

    update(client:Client, id:number) {
        return this.http.put(this.baseUrl+`customers/${id}`, client, { headers: this.headers});
    }

    remove(id:number) {
        return this.http.delete(this.baseUrl+`customers/${id}`, { headers: this.headers});
    }

    anyError(error:any) {
        return this.apiService.getMsgErrorHttp(error);
    }

}
